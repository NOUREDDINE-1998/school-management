// <auto-generated />
namespace SchoolManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CreateFilliereTable1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateFilliereTable1));
        
        string IMigrationMetadata.Id
        {
            get { return "202106072336121_CreateFilliereTable1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
