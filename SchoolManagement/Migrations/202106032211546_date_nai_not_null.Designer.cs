// <auto-generated />
namespace SchoolManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class date_nai_not_null : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(date_nai_not_null));
        
        string IMigrationMetadata.Id
        {
            get { return "202106032211546_date_nai_not_null"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
