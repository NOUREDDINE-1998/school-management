// <auto-generated />
namespace SchoolManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class createClasseClass : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(createClasseClass));
        
        string IMigrationMetadata.Id
        {
            get { return "202106091942179_createClasseClass"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
