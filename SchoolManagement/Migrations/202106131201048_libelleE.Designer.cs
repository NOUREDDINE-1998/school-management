// <auto-generated />
namespace SchoolManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class libelleE : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(libelleE));
        
        string IMigrationMetadata.Id
        {
            get { return "202106131201048_libelleE"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
